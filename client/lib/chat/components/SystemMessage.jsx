import React from "react";
import PropTypes from "prop-types";

const SystemMessage = props => (
  <div className="systemMsg">
    {props.user}
    Jason Statham joined the room.
  </div>
);

SystemMessage.propTypes = {
  user: PropTypes.shape({
    login: PropTypes.string,
    password: PropTypes.string,
    publisher: PropTypes.bool,
  }).isRequired,
};

export default SystemMessage;
