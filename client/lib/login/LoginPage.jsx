import $ from "jquery";
import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import { APIService } from "../sessions";

class LoginPage extends Component {
  componentDidMount() {
    const logInFunction = document.getElementById("logInFunction");

    logInFunction.addEventListener("click", this.logIn);
  }

  logIn = async () => {
    const login = $("#user").val();
    const password = $("#pass").val();
    const body = { login, password };

    try {
      await APIService.post("api/users/login", { body }, "Login WRONG");
      window.user = {
        login,
        password,
        publisher: true,
      };
      this.props.push("/main");
    } catch (e) {
      console.warn("Error. Can't logIn", e);
    }
  };

  onSubmit = e => e.preventDefault();

  render() {
    return (
      <Fragment>
        <div id="main-container" className="container">
          <div id="not-logged" className="vertical-center">
            <div id="img-div">
              <img src="/images/openvidu_grey_bg_transp_cropped.png" />
            </div>
            <form className="form-group jumbotron" onSubmit={this.onSubmit}>
              <p>
                <label>User</label>
                <input
                  className="form-control"
                  type="text"
                  id="user"
                  required
                />
              </p>
              <p>
                <label>Pass</label>
                <input
                  className="form-control"
                  type="password"
                  id="pass"
                  required
                />
              </p>
              <p className="text-center">
                <button id="logInFunction" className="btn btn-lg btn-info">
                  Log in
                </button>
              </p>
            </form>
          </div>
        </div>
        <footer className="footer">
          <div className="container">
            <div className="text-muted">OpenVidu © 2017</div>
            <a href="http://www.openvidu.io/" target="_blank">
              <img
                className="openvidu-logo"
                src="/images/openvidu_globe_bg_transp_cropped.png"
              />
            </a>
          </div>
        </footer>
      </Fragment>
    );
  }
}

export default connect(
  null,
  {
    push,
  },
)(LoginPage);
