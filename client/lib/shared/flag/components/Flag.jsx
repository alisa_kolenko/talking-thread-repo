import React from "react";
import PropTypes from "prop-types";
import Flag, { FlagGroup } from "@atlaskit/flag";
import { colors } from "@atlaskit/theme";
import SuccessIcon from "@atlaskit/icon/glyph/check-circle";
import Error from "@atlaskit/icon/glyph/error";

const FlagComponent = props => {
  const icons = {
    success: <SuccessIcon primaryColor={colors.G300} />,
    error: <Error primaryColor={colors.R300} />,
  };
  return (
    <div>
      <FlagGroup onDismissed={props.onDismiss}>
        {props.flags.map(flag => (
          <Flag
            id={flag.id}
            icon={icons[flag.typeOfFlag]}
            key={flag.id}
            title={flag.title}
            description={flag.description}
            actions={flag.actions}
          />
        ))}
      </FlagGroup>
    </div>
  );
};

FlagComponent.propTypes = {
  onDismiss: PropTypes.func,
  flags: PropTypes.arrayOf(PropTypes.any),
};

export default FlagComponent;
