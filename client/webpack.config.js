const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const PrettierPlugin = require("prettier-webpack-plugin");

module.exports = {
  entry: ["@babel/polyfill", "./lib/index.jsx"],
  output: {
    path: path.resolve("../server/public"),
    filename: "bundle.js",
  },
  mode: "development",

  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve("./lib/index.html"),
    }),
    new PrettierPlugin({
      extensions: [".css", ".js", ".json", ".jsx", ".scss", ".ts"],
    }),
  ],
  resolve: {
    modules: ["./lib", "node_modules"],
    extensions: [".js", ".jsx"],
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-react", "@babel/preset-env"],
            plugins: [
              "@babel/plugin-proposal-class-properties",
              "@babel/plugin-transform-async-to-generator",
            ],
          },
        },
      },
      {
        test: /\.html$/,
        use: ["html-loader"],
      },
      {
        test: /\.scss$/,
        use: ["style-loader", "css-loader", "sass-loader"],
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },

      {
        test: /\.(jpg|png|gif|ico|svg)$/,
        use: [
          {
            loader: "file-loader",
            options: {},
          },
        ],
      },
    ],
  },
};
