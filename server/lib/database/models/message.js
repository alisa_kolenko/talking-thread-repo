module.exports = (sequelize, type) =>
  sequelize.define("message", {
    id: {
      type: type.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    value: {
      type: type.STRING,
    },
  });
