const database = require("../../database");

class GetRooms {
  async execute() {
    let result = false;
    try {
      result = await database.Room.findAll();
    } catch (err) {
      console.log(err);
    }
    return result;
  }
}
module.exports = GetRooms;
