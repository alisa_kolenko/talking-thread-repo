const database = require("../../database");
const MapSessions = require("../MapSessionsService");
const OpenVidu = require("openvidu-node-client").OpenVidu;
const io = require("../../chat/EmitService");

// Environment variable: URL where our OpenVidu server is listening
const OPENVIDU_URL = process.argv[2];
// Environment variable: secret shared with our OpenVidu server
const OPENVIDU_SECRET = process.argv[3];
const OV = new OpenVidu(OPENVIDU_URL, OPENVIDU_SECRET);

class CreateRoom {
  constructor(name) {
    this.roomName = name;
  }
  async execute(body) {
    let result = false;
    const user = await database.User.findOne({
      where: { login: body.login },
      attributes: {
        exclude: ["password"],
      },
    });
    if (user !== null) {
      try {
        const room = await database.Room.create();
        const id = room.id;
        if (this.roomName) {
          room.update({ name: this.roomName });
        } else {
          room.update({ name: "Room" + id });
        }
        const session = await OV.createSession();

        io.createNsp(room.name);

        MapSessions.set(id, session);

        await user.update({ roomId: room.id });

        console.log("Room # " + id + " created by publisher " + user.login);
        result = { roomId: room.id, name: room.name };
      } catch (err) {
        console.log(err);
      }
    } else {
      console.log("User does not exist");
    }
    return result;
  }
}

module.exports = CreateRoom;
