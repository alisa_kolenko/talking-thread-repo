const Joi = require("joi");

const schema = Joi.object().keys({
  id: Joi.number().min(1),
  name: Joi.string(),
  login: Joi.string(),
  password: Joi.string(),
  role: Joi.string(),
  token: Joi.string(),
  roomId: Joi.number(),
});

module.exports = schema;
